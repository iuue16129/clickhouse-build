<?php

include "vendor/autoload.php";

$build = new \ClickHouse\Build([
    'host' => 'localhost',
    'database' => 'default',
    'username' => 'default',
    'password' => '',
    'port' => '8123',
]);


$dataRow = [
    'id' => 'A001',
    'code' => 'C1',
    'create_time' => '2019-05-10 17:00:00'
];

//$result = $build->table('replace_table')->insert($dataRow);

$dataRows = [
    [
        'id' => 'A001',
        'code' => 'C1',
        'create_time' => '2019-05-10 17:00:00'
    ],
    [
        'id' => 'A001',
        'code' => 'C1',
        'create_time' => '2019-05-11 17:00:00'
    ],
    [
        'id' => 'A001',
        'code' => 'C100',
        'create_time' => '2019-05-12 17:00:00'
    ],
    [
        'id' => 'A001',
        'code' => 'C200',
        'create_time' => '2019-05-13 17:00:00'
    ],
    [
        'id' => 'A002',
        'code' => 'C2',
        'create_time' => '2019-05-14 17:00:00'
    ],
    [
        'id' => 'A003',
        'code' => 'C3',
        'create_time' => '2019-05-15 17:00:00'
    ],
];

//$result = $build->table('replace_table')->insertAll($dataRows);

// find
//$data = ->find();

// select
//$list = $build->table('replace_table')->select();

// field limit order
//$field = $build->table('replace_table')->field([
//    'id'
//])->limit(1)->order('id desc')->select();


// where build to raw
//$build->table('replace_table');
//$build->where("(string = string or 1=2)");

// sql : SELECT * FROM replace_table WHERE 1=1  AND (string = string or 1=2) AND `id` = '123' AND `code` IN ( 1,2 ) AND `create_time` BETWEEN '2019-05-10 17:00:00' AND '2019-05-20 17:00:00'
//$build->where([
//    'id'=> '123', // use and
//    'code'=> ['1', '2'], // use in
//    'create_time'=> ['between'=> ['2019-05-10 17:00:00', '2019-05-20 17:00:00']] // use exp: ['exp'=> {$val}]  exp: '>', '>=', '!=', '<>', '<=', '<', '=', 'in', 'not in'
//]);