<?php

namespace ClickHouse\Tests\Build;

use ClickHouse\Build;
use ClickHouse\Tests\TestCase;

class BuildTest extends TestCase
{
    use BuildTrait;

    /**
     * BuildTest constructor.
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    public function __construct()
    {
        parent::__construct();

        $this->initBuild();
    }

    /**
     * build table
     */
    public function testBuildTable()
    {
        $this->build->table('replace_table');

        $this->assertInstanceOf(Build::class, $this->build);
    }

    /**
     * build field
     */
    public function testBuildFiledString()
    {
        $this->build->field('*');
        $this->assertEquals('*', $this->build->getOptions('field'));

        $this->build->field('id, code');
        $this->assertEquals('id, code', $this->build->getOptions('field'));

        $this->build->field([
            'id',
            'code',
            'count()'=> 'cnt'
        ]);
        $this->assertTrue(is_array($this->build->getOptions('field')));

        $this->assertInstanceOf(Build::class, $this->build);

    }

    /**
     * build where
     */
    public function testBuildWhere()
    {
        $this->build->where('1=2'); // use raw sql. sql: 1=1

        $this->build->where([
            'id'=> '123' // sql: id = '123'
        ]);

        $this->build->where([
            'id'=> ['123', '1234'] // sql use in.  sql: id IN (123, 1234)
        ]);

        // sql use exp.
        // exp: '>', '>=', '!=', '<>', '<=', '<', '=', 'in', 'not in', 'between'
        // exp: (in, not in, between) val type is array, other is string
        $this->build->where([
            'create_time'=> ['between'=> ['2019-05-10 17:00:00', '2019-05-20 17:00:00']] // sql: create_time BETWEEN '2019-05-10 17:00:00' AND '2019-05-20 17:00:00'
        ]);

        $this->assertCount(4, $this->build->getOptions('where'));
        $this->assertInstanceOf(Build::class, $this->build);
    }

    /**
     * build group
     */
    public function testBuildGroup()
    {
        // sql: GROUP BY id
        $this->build->group('id');

        $this->assertEquals('id', $this->build->getOptions('group'));
        $this->assertInstanceOf(Build::class, $this->build);
    }

    /**
     * build order
     */
    public function testBuildOrder()
    {
        // sql: ORDER BY id desc
        $this->build->order('id desc');

        $this->assertEquals('id desc', $this->build->getOptions('order'));
        $this->assertInstanceOf(Build::class, $this->build);
    }

    /**
     * build limit
     */
    public function testBuildLimit()
    {
        // sql: LIMIT 2
        $this->build->limit(2);
        $this->assertEquals('2', $this->build->getOptions('limit'));

        // sql: LIMIT 0, 10
        $this->build->limit(0, 10);
        $this->assertEquals('0, 10', $this->build->getOptions('limit'));

        $this->assertInstanceOf(Build::class, $this->build);

    }
}