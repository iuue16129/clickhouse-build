<?php
namespace ClickHouse\Tests\Build;

use ClickHouse\Build;

trait BuildTrait
{
    /**
     * @var Build $build
     */
    private $build;

    /**
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    private function initBuild()
    {
        $config = [
            'host' => 'localhost',
            'database' => 'default',
            'username' => 'default',
            'password' => '',
            'port' => '8123',
        ];
        $this->build = new Build($config);
    }
}