<?php

namespace ClickHouse\Tests\Build;

use ClickHouse\Tests\TestCase;

class SelectTest extends TestCase
{
    use BuildTrait;

    /**
     * SelectTest constructor.
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    public function __construct()
    {
        parent::__construct();

        $this->initBuild();
    }

    /**
     * find test
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    public function testFind()
    {
        $this->build->table('replace_table');

        $row = $this->build->find();

        $data = [
            "id" => "A001",
            "code" => "C1",
            "create_time" => "2019-05-10 17:00:00",
        ];

        $this->assertTrue(is_array($row));
        $this->assertContainsOnly('string', $row);
        $this->assertEquals($data, $row);
    }

    /**
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    public function testSelect()
    {
        $this->build->table('replace_table');

        $rows = $this->build->limit(2)->select();

        $data = [
            [
                "id" => "A001",
                "code" => "C1",
                "create_time" => "2019-05-10 17:00:00",
            ],
            [
                "id" => "A001",
                "code" => "C100",
                "create_time" => "2019-05-12 17:00:00",
            ]
        ];

        $this->assertTrue(is_array($rows));
        $this->assertContainsOnly('array', $rows);
        $this->assertEquals($data, $rows);
    }
}