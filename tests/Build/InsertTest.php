<?php

namespace ClickHouse\Tests\Build;

use ClickHouse\Tests\TestCase;
use ClickHouseDB\Statement;

class InsertTest extends TestCase
{
    use BuildTrait;

    /**
     * InsertTest constructor.
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    public function __construct()
    {
        parent::__construct();

        $this->initBuild();
    }

    /**
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    public function testInsert()
    {
        $dataRow = [
            'id' => 'A001',
            'code' => 'C1',
            'create_time' => '2019-05-10 17:00:00'
        ];

        $this->build->table('replace_table');
        $result = $this->build->insert($dataRow);

        $this->assertInstanceOf(Statement::class, $result);
    }

    /**
     * @throws \ClickHouse\Exception\ClickHouseException
     */
    public function testInsertAll()
    {
        $dataRows = [
            [
                'id' => 'A001',
                'code' => 'C1',
                'create_time' => '2019-05-10 17:00:00'
            ],
            [
                'id' => 'A001',
                'code' => 'C1',
                'create_time' => '2019-05-11 17:00:00'
            ],
            [
                'id' => 'A001',
                'code' => 'C100',
                'create_time' => '2019-05-12 17:00:00'
            ],
            [
                'id' => 'A001',
                'code' => 'C200',
                'create_time' => '2019-05-13 17:00:00'
            ],
            [
                'id' => 'A002',
                'code' => 'C2',
                'create_time' => '2019-05-14 17:00:00'
            ],
            [
                'id' => 'A003',
                'code' => 'C3',
                'create_time' => '2019-05-15 17:00:00'
            ],
        ];

        $this->build->table('replace_table');
        $result = $this->build->insertAll($dataRows);

        $this->assertInstanceOf(Statement::class, $result);
    }
}