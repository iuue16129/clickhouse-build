<?php

namespace ClickHouse\Contracts;

use ClickHouse\Build;
use ClickHouseDB\Statement;

interface ClickHouseInterface
{
//    public function table(string $table): Build;
//
//    public function order(string $order): Build;
//
//    public function group(string $group): Build;
//
//    public function limit(int $limit, ?int $num = null): Build;

    public function where($condition): Build;

    public function insert(array $data): Statement;

    public function insertAll(array $data = []): Statement;

    public function find(): ?array;

    public function select(): array;
}