<?php

namespace ClickHouse;

use ClickHouse\Contracts\ClickHouseInterface;
use ClickHouse\Exception\ClickHouseException;
use ClickHouseDB\Quote\FormatLine;
use ClickHouseDB\Client as ChClient;
use ClickHouseDB\Statement;

/**
 * @method Build table(string $table)
 * @method Build order(string $order)
 * @method Build group(string $group)
 * @method Build field($field)
 * @method Build limit(int $limit, ?int $num = null)
 *
 * Class Build
 * @package app\common\utils\clickhouse
 */
class Build implements ClickHouseInterface
{
    protected $options = [];

    /**
     * @var ChClient
     */
    private $client;

    /**
     * Build constructor.
     * @param $config
     * @throws ClickHouseException
     */
    public function __construct(array $config)
    {
        $this->client = Client::getInstance($config)->getDb();
    }

    /**
     * @param $method
     * @param $args
     * @return $this
     * @throws ClickHouseException
     */
    public function __call($method, $args): self
    {
        if (in_array($method, ['table', 'order', 'group', 'field'])) {
            $this->options[$method] = $args[0];

            return $this;
        }
        if ($method == 'name') {
            $this->options['table'] = $args[0];
            return $this;
        }

        if ($method == 'limit') {
            $this->options['limit'] = isset($args[1]) ? "$args[0], $args[1]" : $args[0];
            return $this;
        }

        throw new ClickHouseException("call method {$method} fail");
    }

    /**
     * where
     * @param string|array $condition
     * @return $this
     */
    public function where($condition): self
    {
        $this->options['where'][] = $condition;

        return $this;
    }

    /**
     * one row
     * @param array $data
     * @return Statement
     * @throws ClickHouseException
     */
    public function insert(array $data): Statement
    {
        $insertSql = '%INSERT% INTO %TABLE% (%FIELD%) VALUES %DATA%';

        $sql = str_replace(
            ['%INSERT%', '%TABLE%', '%FIELD%', '%DATA%'],
            [
                'INSERT',
                $this->parseTable(),
                implode(' , ', array_keys($data)),
                ' (' . FormatLine::Insert($data) . ')'
            ],
            $insertSql);

        return $this->client->transport()->write($sql);
    }

    /**
     * insert rows
     * @param array $dataSet
     * @return Statement
     * @throws ClickHouseException
     */
    public function insertAll(array $dataSet = []): Statement
    {

        $fields = $values = [];

        foreach ($dataSet as $data) {
            if (empty($fields)) {
                $this->options['field'] = $fields = array_keys($data);
            }

            $values[] = ' (' . FormatLine::Insert($data) . ')';
        }

        $insertSql = '%INSERT% INTO %TABLE% (%FIELD%) VALUES %DATA%';

        $sql = str_replace(
            ['%INSERT%', '%TABLE%', '%FIELD%', '%DATA%'],
            [
                'INSERT',
                $this->parseTable(),
                implode(' , ', $fields),
                implode(' , ', $values),
            ],
            $insertSql);

        return $this->client->transport()->write($sql);
    }

    /**
     * find
     * @return array|null
     * @throws ClickHouseException
     * @throws \Exception
     */
    public function find():? array
    {
        $selectSql = 'SELECT%FIELD% FROM %TABLE% %WHERE% %ORDER% LIMIT 1';

        $sql = str_replace(
            ['%TABLE%', '%FIELD%', '%WHERE%', '%ORDER%'],
            [
                $this->parseTable(),
                ' ' . $this->parseField(),
                $this->parseWhere(),
                $this->parseOrder()
            ],
            $selectSql);

        $this->refreshOptions();
        return $this->client->transport()->select($sql)->fetchOne();
    }

    /**
     * select
     * @return array
     * @throws ClickHouseException
     * @throws \Exception
     */
    public function select(): array
    {
        $selectSql = 'SELECT%FIELD% FROM %TABLE% %WHERE% %GROUP% %HAVING% %ORDER% %LIMIT%';

        $sql = str_replace(
            ['%TABLE%', '%FIELD%', '%WHERE%', '%GROUP%', '%HAVING%', '%ORDER%', '%LIMIT%'],
            [
                $this->parseTable(),
                ' ' . $this->parseField(),
                $this->parseWhere(),
                $this->parseGroup(),
                $this->parseHaving(),
                $this->parseOrder(),
                $this->parseLimit()
            ],
            $selectSql);
        $this->refreshOptions();
        return $this->client->transport()->select($sql)->rows();
    }

    /**
     * @return string
     */
    private function parseGroup(): string
    {
        return isset($this->options['group']) ? 'GROUP BY ' . $this->options['group'] : '';
    }

    /**
     * @return string
     */
    private function parseOrder(): string
    {
        return isset($this->options['order']) ? 'ORDER BY ' . $this->options['order'] : '';
    }

    /**
     * @return string
     */
    private function parseLimit(): string
    {
        return isset($this->options['limit']) ? 'LIMIT ' . $this->options['limit'] : '';
    }

    /**
     * @return string
     * @throws ClickHouseException
     */
    private function parseTable(): string
    {
        if (!isset($this->options['table']) || !is_string($this->options['table'])) {
            throw new ClickHouseException('query no set table');
        }

        return $this->options['table'];
    }

    /**
     * @return string
     */
    private function parseField(): string
    {
        if (!isset($this->options['field'])) {
            return '*';
        } elseif (is_string($this->options['field'])) {
            return $this->options['field'];
        } elseif (is_array($this->options['field'])) {
            $array = [];

            foreach ($this->options['field'] as $key => $field) {
                if (!is_numeric($key)) {
                    $array[] = "{$this->parseKey($key)} AS {$this->parseKey($field)}";
                } else {
                    $array[] = $this->parseKey($field);
                }
            }

            return implode(',', $array);
        }

        return '*';
    }

    /**
     * parse key
     * @param $key
     * @return string
     */
    private function parseKey($key): string
    {
        if ('*' != $key && !preg_match('/[,\'\"\*\(\)`.\s]/', $key)) {
            $key = '`' . $key . '`';
        }

        return $key;
    }

    /**
     * parse val
     * @param $val
     * @return string
     */
    private function parseVal($val): string
    {
        return "'" . addslashes($val) . "'";
    }

    /**
     * parse having
     * @return string
     */
    private function parseHaving(): string
    {
        $where = isset($this->options['having']) ? $this->options['having'] : '';
        $whereStr = $this->buildWhere($where);

        return empty($whereStr) ? '' : "HAVING {$whereStr}";
    }

    /**
     * parse where
     * @return string
     */
    private function parseWhere(): string
    {
        $where = isset($this->options['where']) ? $this->options['where'] : '';
        $whereStr = $this->buildWhere($where);

        return empty($whereStr) ? '' : "WHERE {$whereStr}";
    }

    /**
     * build condition
     * @param $where
     * @return string
     */
    private function buildWhere($where): string
    {
        if (empty($where)) {
            return '';
        }

        $whereStr = '1=1 ';
        foreach ($where as $value) {
            if (is_string($value)) {
                $whereStr .= " AND {$value}";
                continue;
            }

            foreach ($value as $key => $val) {
                if (!is_array($val)) {
                    $whereStr .= " AND {$this->parseKey($key)} = {$this->parseVal($val)}";
                }

                if (is_array($val)) {
                    if (count($val) == 0) {
                        $whereStr .= " AND {$this->parseKey($key)} = ''";
                    }

                    $exp = strtoupper(key($val));
                    // 纯数组 执行in操作
                    if (is_numeric($exp)) {
                        $whereStr .= $this->parseIn('IN', $key, $val);
                        continue;
                    }

                    if (in_array($exp, ['>', '>=', '!=', '<>', '<=', '<', '='], true)) {
                        $whereStr .= " AND {$this->parseKey($key)} {$exp} {$this->parseVal(current($val))}";
                        continue;
                    }

                    if (in_array($exp, ['IN', 'NOT IN'], true)) {
                        $whereStr .= $this->parseIn($exp, $key, $val);
                        continue;
                    }

                    if (in_array($exp, ['BETWEEN', 'NOT BETWEEN'])) {
                        list($start, $end) = current($val);

                        $whereStr .= " AND {$this->parseKey($key)} {$exp} {$this->parseVal($start)} AND {$this->parseVal($end)}";
                        continue;
                    }

                    if($exp == 'LIKE'){
                        $whereStr  .= " AND {$this->parseKey($key)} {$exp} {$this->parseVal($val[key($val)])}";
                    }
                }
            }
        }
        return $whereStr;
    }

    /**
     * parse in
     * @param $exp
     * @param $key
     * @param $value
     * @return string
     */
    private function parseIn($exp, $key, $value): string
    {
        if (count($value) == 1) {
            return $this->parseKey($key) . $exp == 'IN' ? " = " : ' <> ' . $this->parseVal($value[0]);
        }
        $zone = implode(',', $value);
        $value = empty($zone) ? "''" : $zone;

        return " AND {$this->parseKey($key)} {$exp} ( $value )";
    }

    private function refreshOptions()
    {
        $this->options = [];
    }

    /**
     * @return ChClient
     */
    public function getClient(): ChClient
    {
        return $this->client;
    }

    /**
     * @param string|null $key
     * @return mixed|null
     */
    public function getOptions(?string $key=null)
    {
        return $this->options[$key] ?? null;
    }
}