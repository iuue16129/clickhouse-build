<?php
namespace ClickHouse\Exception;

use Throwable;

class ClickHouseException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = "ClickHouse: " . $message;
        parent::__construct($message, $code, $previous);
    }
}