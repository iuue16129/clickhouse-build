<?php

namespace ClickHouse;

/**
 * @author wfczdmy16129@163.com
 * Class Buffer
 * @package app\common\utils\clickhouse
 */
class Buffer
{
    // 最小执行行
    private $minRows = 10;

    // 最大执行行
    private $maxRows = 20;

    // 最小执行时间
    private $minTime = 10;

    // 最大执行时间
    private $maxTime = 20;

    // 开始计算时间
    private $startTime = 0;

    // 当前行数
    private $dataRowNum = 0;

    /**
     * Buffer constructor.
     * @param int $minRows
     * @param int $maxRows
     * @param int $minTime
     * @param int $maxTime
     */
    public function __construct($minRows = 10, $maxRows = 20, $minTime = 10, $maxTime = 20)
    {
        $this->minRows = $minRows;
        $this->maxRows = $maxRows;
        $this->minTime = $minTime;
        $this->maxTime = $maxTime;
        $this->refresh();
    }

    /**
     * 确认当前状态  (dataRowNum >= minRows && time >= minTime) || maxTime || maxRows
     * @return bool
     */
    public function checkBuffer(): bool
    {
        if ($this->dataRowNum >= $this->maxRows) {
            //dump('rows: ' . $this->dataRowNum . ' 用时: ' . bcsub(time(), $this->startTime));
            $this->refresh();
            return true;
        }

        if ($this->startTime + $this->maxTime < time()) {
            //dump('超时, 当前数量 '.$this->dataRowNum);
            $this->refresh();
            return true;
        }

        if ($this->startTime + $this->minTime < time() && $this->dataRowNum >= $this->minRows) {
            //dump("num: {$this->dataRowNum}, 用时: " . bcsub(time(), $this->startTime));
            $this->refresh();
            return true;
        }

        return false;
    }

    public function rowNumInc()
    {
        ++$this->dataRowNum;
    }

    /**
     * 刷新时间
     */
    private function refresh()
    {
        $this->startTime = time();
        $this->dataRowNum = 0;
    }
}