<?php

namespace ClickHouse;

use ClickHouse\Exception\ClickHouseException;
use ClickHouseDB\Client as ChClient;

class Client
{
    private $db;

    /**
     * Client constructor.
     * @param $config
     * @throws ClickHouseException
     */
    private function __construct(array $config)
    {
        $this->db = new ChClient($config);

        if ($this->db->ping() !== true) {
            throw new ClickHouseException('init click house fail');
        }

        $this->db->database($config['database']);
    }

    /**
     * @var self
     */
    private static $instance;

    /**
     * @param $config
     * @return Client
     * @throws ClickHouseException
     */
    public static function getInstance(array $config): self
    {
        if (!(self::$instance instanceof Client)) {
            self::$instance = new self($config);
        }

        return self::$instance;
    }

    /**
     * @return ChClient
     */
    public function getDb(): ChClient
    {
        return $this->db;
    }
}